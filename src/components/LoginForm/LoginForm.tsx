import { Eye, EyeOff } from 'lucide-react'
import { useState } from 'react'
import { useForm } from 'react-hook-form'
import CustomInput from '../ui/CustomInput/CustomInput'
import ErrorMessage from '../ui/ErrorMessage'
import styles from './LoginForm.module.scss'
interface FormState {
    login: string
    password: string
}

const LoginForm = () => {
    const {
        handleSubmit,
        register,
        formState: { errors },
    } = useForm<FormState>({ mode: 'onChange' })
    const [loginError, setLoginError] = useState<string | null>(null)
    const [showPassword, setShowPassword] = useState<boolean>(false)

    const handleLogin = async (data: FormState) => {
        try {
            //axios сервис авторизации
            setLoginError(null)
        } catch (error) {
            setLoginError('Неверный логин или пароль')
        }
    }
    return (
        <div className={styles.container}>
            <div className={styles.form}>
                <h1 className={styles.title}>Авторизация</h1>
                <form onSubmit={handleSubmit(handleLogin)}>
                    <div>
                        <CustomInput
                            id="login"
                            label="Введите логин:"
                            {...register('login', {
                                required: 'Обязательное поле',
                            })}
                        />
                    </div>
                    <ErrorMessage error={errors.login} />
                    <div className={styles.checkPassContainer}>
                        <div>
                            <CustomInput
                                id="password"
                                label="Введите пароль:"
                                type={showPassword ? 'text' : 'password'}
                                {...register('password', {
                                    required: 'Обязательное поле',
                                })}
                            />
                        </div>

                        <button
                            type="button"
                            className={styles.togglePassword}
                            onClick={() => setShowPassword((prev) => !prev)}
                        >
                            {showPassword ? (
                                <Eye size={25} />
                            ) : (
                                <EyeOff size={25} />
                            )}
                        </button>
                    </div>
                    <ErrorMessage error={errors.password} />

                    <div className={styles.btnContainer}>
                        <button className={styles.loginBtn} type="submit">
                            Войти
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default LoginForm

import React from 'react'
import { Review } from '../entites/Review'

const ReviewItem: React.FC<{ data: Review }> = ({ data }) => {
    return (
        <div key={data.ID}>
            <h4>{data.Username}</h4>
            <p>Рейтинг: {data.Rating}</p>
            <p>Дата: {data.Date.toString()}</p>
            <p>{data.Content}</p>
        </div>
    )
}

export default ReviewItem

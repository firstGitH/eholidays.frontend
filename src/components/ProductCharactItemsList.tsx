import { FC } from 'react'
import { ProductItem } from '../entites/Product'
import ProductCharactItem from './ProductCharactItem'

const ProductCharactItemsList: FC<{ data: ProductItem[] | undefined }> = ({
    data,
}) => {
    return (
        <div>
            <h1>ProductCharactItemsList</h1>
            {data?.map((item) => (
                <ProductCharactItem data={item} />
            ))}
        </div>
    )
}

export default ProductCharactItemsList

import { useEffect, useState } from 'react'
import ReactPaginate from 'react-paginate'
import { useParams } from 'react-router-dom'
import { Product } from '../entites/Product'
import ProductService from '../shared/services/product.service'

const ProductsList = () => {
    const [products, setProducts] = useState<Product[]>([])
    const [offset, setOffset] = useState(0)
    const [perPage, setPerPage] = useState(10)
    const [pageCount, setPageCount] = useState(0)

    const { id } = useParams()

    const fetchProducts = async () => {
        try {
            const response = await ProductService.getProducts(offset, perPage)
            const data = await response.data

            const totalResponse = await ProductService.getTotalProducts()
            const total = await totalResponse.data

            setProducts((prevProducts) => [
                ...new Set([...prevProducts, ...data]),
            ])
            setPageCount(Math.ceil(total / perPage))
        } catch (error) {
            console.error(error)
        }
    }

    useEffect(() => {
        fetchProducts()
    }, [offset, perPage])

    return (
        <div>
            {products.map((product, index) => (
                <div key={index}>
                    <h2>{product.Name}</h2>
                    <img
                        src={`data:image/jpeg;base64,${product.Image.Data}`}
                        alt={product.Image.Name}
                    />
                    <p>Price: {product.Price}</p>
                    <p>Rating: {product.Rating}</p>
                </div>
            ))}
            <ReactPaginate
                previousLabel={'предыдущая'}
                nextLabel={'следующая'}
                breakLabel={'...'}
                breakClassName={'break-me'}
                pageCount={pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={(selectedItem) => {
                    setOffset(selectedItem.selected * perPage)
                }}
                containerClassName={'pagination'}
                activeClassName={'active'}
            />
        </div>
    )
}

export default ProductsList

import React from 'react'
import { Subscription } from '../entites/Subscription'

const SubscriptionItem: React.FC<{ data: Subscription }> = ({ data }) => {
    return (
        <div key={data.ID}>
            <h4>{data.ProductName}</h4>
            <p>Подиcан с: {data.CreatedDate.toString()}</p>
        </div>
    )
}

export default SubscriptionItem

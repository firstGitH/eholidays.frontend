import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Product } from '../entites/Product'
import { URLS } from '../shared/config/api-routes'
const ProductItem: React.FC<{ data: Product }> = ({ data }) => {
    const navigate = useNavigate()
    return (
        <div>
            <div key={data.ID}>
                <h2>{data.Name}</h2>
                <p>Цена: {data.Price}</p>
                <p>Рейтинг: {data.Rating}</p>
                <img
                    src={`data:image/jpeg;base64,${data.Image.Data}`}
                    alt={data.Image.Name}
                />
                <button
                    onClick={
                        () => navigate(`${URLS.PRODUCTS}/${data.ID}`)
                        /* TODO: go to ProductInfoPage */
                    }
                >
                    Подробнее
                </button>
            </div>
        </div>
    )
}

export default ProductItem

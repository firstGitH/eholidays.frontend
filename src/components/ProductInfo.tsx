import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { ProductInfo } from '../entites/Product'
import ProductService from '../shared/services/product.service'

const ProductInfo = () => {
    const { id } = useParams()
    const [data, setData] = useState({})

    useEffect(() => {
        const fetchProduct = async () => {
            const response = await ProductService.getProductFullInfo(id)
            setData(response.data)
        }

        fetchProduct()
    }, [id])

    return (
        <div key={data.ID}>
            <h1>ProductInfo</h1>
            <p>{data.}</p>
            <p>Название: {data.Name}</p>
            <p>Цена: {data.Price}</p>
            <p>Описание: {data.Description}</p>
            <p>Количество: {data.Quantity}</p>
            <p>Рейтинг: {data.Rating}</p>
            <p>Добавлен: {data.CreatedDate.getTime()}</p>
            {data.UpdatedDate ? <p>Обновлен: </p> : null}

            {/* <p>{data.d}</p> */}
        </div>
    )
}

export default ProductInfo

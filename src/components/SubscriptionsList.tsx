import { FC } from 'react'
import { Subscription } from '../entites/Subscription'
import SubscriptionItem from './SubscriptionItem'

const SubscriptionsList: FC<{ data: Subscription[] | undefined }> = ({
    data,
}) => {
	
    return (
        <div>
            <h1>Subscriptions List</h1>
            {data?.map((item) => (
                <SubscriptionItem data={item} />
            ))}
        </div>
    )
}

export default SubscriptionsList

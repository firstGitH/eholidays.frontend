import React from 'react'
import { Notification } from '../entites/Notification'

const NotificationItem: React.FC<{ notification: Notification }> = ({
    notification,
}) => {
    return (
        <div key={notification.ID}>
            <h2>{notification.Title}</h2>
            <p>Тип: {notification.Type}</p>
            <p>Дата: {notification.Date.toString()}</p>
            <p>{notification.Content}</p>
        </div>
    )
}

export default NotificationItem

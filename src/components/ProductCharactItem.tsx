import React from 'react'
import { ProductItem } from '../entites/Product'

const ProductCharactItem: React.FC<{ data: ProductItem }> = ({ data }) => {
    return (
        <div key={data.ID}>
            <h4>Характеристика: {data.Name}</h4>
            <p>Значение: {data.Value}</p>
        </div>
    )
}

export default ProductCharactItem

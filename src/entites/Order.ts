export interface Order {
    ID: number
    UserID: number
    CreatedDate: Date
    TotalCost: number
    Status: string
    PaymentMethod: string
    DeliveryLocation: string
    UpdatedDate: Date
}

export interface OrderItem {
    ID: number
    OrderID: number
    ProductID: number
    Quantity: number
    Price: number
    Name: string
    CurrentPrice: number
}

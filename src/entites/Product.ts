export interface Product {
    ID: number
    CategoryID: number
    Name: string
    Price: number
    Rating: number
    Image: ProductImage
}

export interface ProductInfo {
    ID: number
    CategoryID: number
    Name: string
    Price: number
    Description: string
    Quantity: number
    Rating: number
    Image: ProductImage
    CreatedDate: Date
    UpdatedDate: Date
    ItemCount: number
    SubscriptionCount: number
    ReviewCount: number
    NotificationCount: number
}

export interface ProductImage {
    Name: string
    Data: string // Данные изображения в формате base64
}

export interface ProductItem {
    ID: number
    ProductID: number
    Name: string
    Value: string
}

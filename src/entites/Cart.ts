export interface CartItem {
    ID: number
    UserID: number
    ProductID: number
    Name: string
    Price: number
}

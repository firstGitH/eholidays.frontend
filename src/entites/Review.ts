export interface Review {
    ID: number
    UserID: number
    ProductID: number
    RatingID: number
    Username: string
    Rating: number
    Date: Date
    Content: string
}

export interface Notification {
    ID: number
    ProductID: number
    Type: string
    Date: Date
    Title: string
    Content: string
}

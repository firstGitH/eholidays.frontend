export interface Subscription {
    ID: number
    UserID: number
    ProductID: number
    ProductName: string
    CreatedDate: Date
}

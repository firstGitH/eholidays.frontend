import { useState } from 'react'
import ProductCharactItemsList from '../components/ProductCharactItemsList'
import ProductInfo from '../components/ProductInfo'
import ReviewsList from '../components/ReviewsList'
import { ProductItem } from '../entites/Product'
import { Review } from '../entites/Review'

function ProductInfoPage() {
    const [countReviews, setCountReviews] = useState(0)

    var productCharats: ProductItem[] = []
    var reviews: Review[] = []

    return (
        <div>
            <h1>Product Info Page</h1>
            <ProductInfo />
            <ProductCharactItemsList data={productCharats} />
            <ReviewsList />
        </div>
    )
}

export default ProductInfoPage

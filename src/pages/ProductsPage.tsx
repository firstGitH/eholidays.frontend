import ProductsList from '../components/ProductsList'
import { Product } from '../entites/Product'

const ProductsPage = () => {
    var products: Product[] = []
    return (
        <div>
            <h1>Products Page</h1>
            <ProductsList data={products} />
        </div>
    )
}

export default ProductsPage

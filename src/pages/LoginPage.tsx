import LoginForm from '../components/LoginForm/LoginForm'

const LoginPage = () => {
    return (
        <div className="mt-40">
            <LoginForm />
        </div>
    )
}

export default LoginPage

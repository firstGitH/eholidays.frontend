import { Product, ProductInfo } from '../../entites/Product'
import instance from '../api/api.instance'
import { URLS } from '../config/api-routes'

const ProductService = {
    async getProductFullInfo(id: string | undefined) {
        return instance.get<ProductInfo>(`${URLS.PRODUCTS}/${id}`)
    },

    async getProducts(offset: number | string, limit: number | string) {
        return instance.get<Product[]>(
            `${URLS.PRODUCTS}/?offset=${offset}&limit=${limit}`
        )
    },
    async getTotalProducts() {
        return instance.get<number>(`${URLS.PRODUCTS}/?total`)
    },

    async getProductsByCategory(
        categoryId: number | string,
        offset: number | string,
        limit: number | string
    ) {
        return instance.get<Product[]>(
            `${URLS.PRODUCTS}/?categoryId=${categoryId}&offset=${offset}&limit=${limit}`
        )
    },

    async getTotalProductsByCategory(categoryId: number | string) {
        return instance.get<number>(
            `${URLS.PRODUCTS}/totalByCategory?categoryId=${categoryId}`
        )
    },
}

export default ProductService

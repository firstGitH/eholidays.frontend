import { Route, Routes } from 'react-router-dom'
import HomePage from '../pages/HomePage'
import LoginPage from '../pages/LoginPage'
function App() {
    return (
        <Routes>
            <Route path={'/'} element={<HomePage />} />
            <Route path={'/auth/login'} element={<LoginPage />} />
            <Route
                path={'*'}
                element={
                    <>
                        <div>Eroor</div>
                    </>
                }
            />
        </Routes>
    )
}

export default App
